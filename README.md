
XCanasta
----------

XCanasta is an online multiplayer adaptation of the card game canasta 
([Wikipedia](https://en.wikipedia.org/wiki/Canasta)).

<b>ATTENTION: The game is not yet playable. I am still looking for developers who will support me in completing it.</b>

Screenshot:
-----------

[![Screenshot](xcanasta/guiclient/images/screenshot.jpg)](xcanasta/guiclient/images/images/screenshot.jpg)

Dependencies:
-------------
* Python
* [Twisted](https://twistedmatrix.com)
* [wxPython](http://www.wxpython.org/)

The code has been tested with the following versions:
* Python 3.8.5
* Twisted-18.9.0
* wxPython-4.0.7

get started:
------------

<h3>For Ubuntu</h3>
<code>sudo apt install python3-wxgtk4.0</code> (only for the client)<br>
<code>sudo apt install python3-twisted</code>

* Run Server:<br>
  python3 xcanasta/xcanasta-server.py

* Run Client:<br>
  python3 xcanasta/xcanasta-client.py [ip-adress] [player] [password] [gamename]<br>
  (The parameters are optional.)

<h3>For Windows</h3>
Check, if you have Python3 installed:<br>
<code>python --version</code><br>
Python 3.8.5
<br><br>
Install the libraries, you need administration rights:<br>
<code>pip install wxpython</code> (only for the client)<br>
<code>pip install twisted</code>

* Run Server:<br>
  python xcanasta/xcanasta-server.py

* Run Client:<br>
  python xcanasta/xcanasta-client.py [ip-adress] [player] [password] [gamename]<br>
  (The parameters are optional.)

