#!/usr/bin/env python

#  XCanasta -- Network-compatible computer variant of the popular card game
#  Copyright (C) 2021 Horst Aldebaran
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License, Version 2, as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import gettext, sys, os
import locale
import server

try:
    cwd=os.getcwd()
    if "LANGUAGE" in os.environ:
       trans = gettext.translation("xcanasta", cwd+"/xcanasta/locale", [os.environ['LANGUAGE']])
       trans.install()
    print (_("Attempting to launch server from current directory..."))
except:
    gettext.install("xcanansta","locale") # use system locale dir if client was installed
    print(_("abort"))

server.init()
print(_("App closed..."))


