#  XCanasta -- Network-compatible computer variant of the popular card game
#  Copyright (C) 2021 Horst Aldebaran
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License, Version 2, as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA



import os, sys, string, gettext, wx
import random
from .TextPanel import *

class CardWindow(wx.ScrolledWindow):
   def __init__(self, parent):
      wx.ScrolledWindow.__init__(self, parent)

      cwd=os.getcwd()
      # load in the ticket images
      self.ticketImages = []
      self.card         = []
      for i in list(range(53)):
         if i<10: 
            filename = cwd+"/xcanasta/guiclient/images/card0" + str(i) + ".jpg"
         else:   
            filename = cwd+"/xcanasta/guiclient/images/card" + str(i) + ".jpg"
         self.ticketImages.append(wx.Image(filename, wx.BITMAP_TYPE_ANY))
         strcard=self.cardvalue(i)
#         self.card.append("card"+repr(i)+" "+strcard)
         self.card.append(strcard)

      # the toplevel sizer is this two-column FlexGridSizer;
      # the left column is filled with vertical wx.BoxSizers,
      # each of which places the turn number above Mr. X's
      # known locations.
      self.ticketSizer = wx.FlexGridSizer(15, 2, 0, 0)

      self.vbSizers     = []
      self.turns        = []
      self.locations    = []
      self.tickets      = []
      # left column
      self.panels       = []
      self.panelSizers  = []
      # right column
      self.panels2      = []
      self.panelSizers2 = []
      self.randomnum = set()
      for i in list(range(15)):
         self.panels.append(wx.Panel(self, -1, wx.DefaultPosition, wx.DefaultSize, wx.SIMPLE_BORDER))
         # TRANSLATORS: this is for the turn number labels in the history window
         if i<10:
            self.turns.append(TextPanel(self.panels[i], _(" 0%(num)d ") % {"num" : i+1},
            12, 0))
         else:   
            self.turns.append(TextPanel(self.panels[i], _(" %(num)d ") % {"num" : i+1},
            12, 0))
         self.locations.append(TextPanel(self.panels[i], " ", 16, 0))

         self.vbSizers.append(wx.BoxSizer(wx.VERTICAL))
         self.vbSizers[i].Add(self.turns[i], 1, wx.EXPAND|wx.CENTRE|wx.ADJUST_MINSIZE)
         self.vbSizers[i].Add(self.locations[i], 1, wx.EXPAND|wx.CENTRE|wx.ADJUST_MINSIZE)

#         self.panelSizers.SetMinSize(wx.Size(200, 500))
         self.panelSizers.append(wx.BoxSizer(wx.HORIZONTAL))
         self.panelSizers[i].Add(self.vbSizers[i], 1, wx.EXPAND)
         self.panels[i].SetSizerAndFit(self.panelSizers[i])

         self.panels2.append(wx.Panel(self, -1, wx.DefaultPosition, wx.DefaultSize, wx.SIMPLE_BORDER))
#         self.tickets.append(wx.StaticBitmap(self.panels2[i], -1, wx.Bitmap(self.ticketImages[4])))
         self.intRandomNum=self.getRandomNum(i)
         self.tickets.append(wx.StaticBitmap(self.panels2[i], -1, wx.Bitmap(self.ticketImages[self.intRandomNum])))
#         self.card.Add(self.intRandomNum)
#         self.tickets[i].Bind(wx.EVT_LEFT_DCLICK, self.cardChoice)
#         self.tickets[i].Bind(wx.EVT_LEFT_DOWN, self.cardChoice)
#         self.tickets[i].Bind(wx.EVT_LEFT_DOWN, lambda event: self.cardChoice(event, cardnr=i))
         lmda = lambda event, y = i, intRnd = self.intRandomNum: self.cardChoice(event, cardnr=int(y), card=self.card[intRnd])
         self.tickets[i].Bind(wx.EVT_LEFT_DOWN, lmda )         

         self.panelSizers2.append(wx.BoxSizer(wx.HORIZONTAL))
         self.panelSizers2[i].Add(self.tickets[i], 1, wx.EXPAND)
         self.panels2[i].SetSizerAndFit(self.panelSizers2[i])

         self.ticketSizer.Add(self.panels[i], 0, wx.EXPAND|wx.CENTRE|wx.LEFT|wx.TOP, 5)
         self.ticketSizer.Add(self.panels2[i], 0, wx.EXPAND|wx.CENTRE|wx.LEFT|wx.TOP|wx.RIGHT, 5)

      self.SetSizer(self.ticketSizer)

      self.ticketSizer.Fit(self)
      self.SetScrollRate(0, 10)
      
   def cardChoice(self, event, cardnr, card):    
      alert = wx.MessageDialog(self, _("Do you want to choice this card? "+card),
         _("Cardchoice"), wx.YES_NO|wx.ICON_EXCLAMATION)
      if alert.ShowModal() == wx.ID_YES:
         ret = wx.MessageDialog(self, _("You have chosen "+card),
         _("Cardchoice"), wx.YES_NO|wx.ICON_EXCLAMATION)
         ret.ShowModal()

   def cardvalue(self, cardnr):
   	# Pik=Pike (Spade)
   	# Herz=Heart
   	# Karo=Tile (Diamond)
   	# Kreuz=Clover (Club)
      switcher = {
        0: "Pike 2",
        1: "Pike 3",
        2: "Pike 4",
        3: "Pike 5",
        4: "Pike 6",
        5: "Pike 7",
        6: "Pike 8",
        7: "Pike 9",
        8: "Pike 10",
        9: "Pike Jack",
        10: "Pike Queen",
        11: "Pike King",
        12: "Pike Ace",
        13: "Tile 2",
        14: "Tile 3",
        15: "Tile 4",
        16: "Tile 5",
        17: "Tile 6",
        18: "Tile 7",
        19: "Tile 8",
        20: "Tile 9",
        21: "Tile 10",
        22: "Tile Jack",
        23: "Tile Queen",
        24: "Tile King",
        25: "Tile Ace",
        26: "Clover 2",
        27: "Clover 3",
        28: "Clover 4",
        29: "Clover 5",
        30: "Clover 6",
        31: "Clover 7",
        32: "Clover 8",
        33: "Clover 9",
        34: "Clover 10",
        35: "Clover Jack",
        36: "Clover Queen",
        37: "Clover King",
        38: "Clover Ace",
        39: "Heart 2",
        40: "Heart 3",
        41: "Heart 4",
        42: "Heart 5",
        43: "Heart 6",
        44: "Heart 7",
        45: "Heart 8",
        46: "Heart 9",
        47: "Heart 10",
        48: "Heart Jack",
        49: "Heart Queen",
        50: "Heart King",
        51: "Heart Ace",
        52: "Joker",
      }           
      return switcher.get(cardnr, "Invalid card")      	   	 

   def getRandomNum(self, i):
      randomnum=random.randint(0,52)
      if len(self.randomnum)>0:
         gef = False
         while gef == False:
            gef = True 	
            for z in list(self.randomnum):
               if z == randomnum:
                  gef = False
                  randomnum=random.randint(0,52)
      self.randomnum.add(randomnum)       	
      return randomnum   	


   # update one of the location numbers
   def setLocation(self, turnNum, locStr):
      print("historywindow - setlocation")  
      print(turnNum)
      print(self.vbSizers[turnNum].IsEmpty())
      if self.vbSizers[turnNum].IsEmpty() == False:
         print(self.vbSizers[turnNum].GetItemCount())
      self.vbSizers[turnNum].Detach(self.locations[turnNum])
      self.locations[turnNum].Destroy()
      fontWeight = wx.NORMAL
      self.locations[turnNum] = TextPanel(self.panels[turnNum], " " + locStr + " ",
         16, 0, fontWeight)
      self.vbSizers[turnNum].Add(self.locations[turnNum], 1, wx.EXPAND|wx.CENTRE)

      self.panelSizers[turnNum].Layout()


   # update one of the ticket graphics 
   def setTicket(self, turnNum, tickNum):
      self.tickets[turnNum].SetBitmap(wx.Bitmap(self.ticketImages[tickNum]))
      self.tickets[turnNum].Refresh(False)





