#  XCanasta -- Network-compatible computer variant of the popular card game
#  Copyright (C) 2021 Horst Aldebaran
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License, Version 2, as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA




# MoveDialog.py
#
# This class generates a wx.Dialog that is used to choose a move (or double move).
# Drop-down boxes allow the player to choose from the possible moves.  The code
# is rather lengthy because the MoveDialog validates itself on-the-fly: that is,
# the transportation drop-down box always shows only transports that are available
# for the currently selected destionation, etc.

# FIXME: this appears to be correct, but needs some major cleanup.  Maybe some portions
#        can be factored better into functions.

import gettext, wx
from common.map import *



DIALOGDESTROYED = wx.NewEventType() 
 
def EVT_DIALOGDESTROYED(window, function): 
    """Your documentation here""" 
    window.Connect(-1, -1, DIALOGDESTROYED, function)
 
class DialogDestroyedEvent(wx.PyCommandEvent): 
    eventType = DIALOGDESTROYED
    def __init__(self, windowID): 
        wx.PyCommandEvent.__init__(self, self.eventType, windowID) 
 
    def Clone(self): 
        self.__class__(self.GetId()) 


class MoveDialog(wx.Dialog):
   # currPos is an integer indicating the current player position
   # destPos is a first choice destination, which the player probably
   #    chose by clicking on a map location
   # players is a list of locations and tokens for all players
   # playerIdx is the index if this player in that list
   # messenger passes messages to the network interface
   # destroyedCallback informs the parent that this dialog is destroyed
   # Use EVT_GOT_MOVE to catch the return values (insert derogatory comment about
   #    lack of flexibility in return values for wxDialog::EndModal())
   def __init__(self, parent, ID, destPos, playerList, playerIdx, messenger, destroyedCallback):
      title=_("Choose a Move")
      if playerIdx == 1:
         title=title+" - "+_("Red")
      elif playerIdx == 2:
         title=title+" - "+_("Yellow")
      elif playerIdx == 3:
         title=title+" - "+_("Green")
      elif playerIdx == 4:
         title=title+" - "+_("Blue")
      elif playerIdx == 5:
         title=title+" - "+_("Black")
      else:
         title=title+" - "+_("Purple")
      wx.Dialog.__init__(self, parent, ID, title)
      self.parent = parent

      self.panel = wx.Panel(self, -1)

      self.playerIdx         = playerIdx
      self.playerList        = playerList
      self.currPos           = self.playerList[self.playerIdx][1]
      self.messenger         = messenger
      self.destroyedCallback = destroyedCallback

      # TRANSLATORS: this is a label for a choose box that lets the user select single or double move
#      self.moveType = wx.RadioBox(self.panel, -1, _("move type: "), wx.DefaultPosition, wx.DefaultSize,
#            # TRANSLATORS: as in "single move", not "double move"
#            [_("Card joint"), 
#            # TRANSLATORS: as in "double move", not "single move"
#            _("Discard pile")], 1, wx.RA_SPECIFY_ROWS)

      self.moves, self.movesStr = self.getAvailMoves(self.currPos, self.playerList, self.playerIdx)
      if destPos in self.moves:
         self.Show(True)
      else:
         self.Show(False)
         if destPos != 0:
            self.drawMoveErrorDialog()
      

      self.trans1ID = wx.NewId()
      self.trans    = []
      self.transStr = []

      pl2 = []
      player = []
      player.append(self.playerList[0][0])
      tokenList = self.playerList[0][2][:]
      player.append(tokenList)
      pl2.append(player)
      for i in list(range(1, len(self.playerList))):
         pl2.append(self.playerList[i])

      self.dest2ID = wx.NewId()
      self.moves2    = []
      self.moves2Str = []

      self.trans2ID = wx.NewId()
      self.trans2    = []
      self.trans2Str = []


      labelFont = wx.Font(self.GetFont().GetPointSize(), wx.DEFAULT, wx.NORMAL, wx.BOLD)
      labelFont.SetWeight(wx.BOLD)

      okButton = wx.Button(self.panel, wx.ID_OK, _("OK"))
      cancelButton = wx.Button(self.panel, wx.ID_CANCEL, _("Cancel"))

      buttonSizer = wx.BoxSizer(wx.HORIZONTAL)
      buttonSizer.Add(cancelButton, 0, wx.ALIGN_CENTRE | wx.ALL, 5)
      buttonSizer.Add(okButton, 0, wx.ALIGN_CENTRE | wx.ALL, 5)

      self.pSizer = wx.BoxSizer(wx.VERTICAL)
#      self.pSizer.Add(self.moveType, 0, wx.ALIGN_CENTRE | wx.ALL, 5)
      self.pSizer.Add(buttonSizer, 0, wx.ALIGN_RIGHT | wx.ALL, 5)

      self.panel.SetSizer(self.pSizer)
      self.pSizer.Fit(self.panel)
      self.sizer = wx.BoxSizer(wx.VERTICAL)
      self.sizer.Add(self.panel, 1, wx.EXPAND | wx.ALL | wx.ADJUST_MINSIZE, 5)
      self.SetSizer(self.sizer)
      self.sizer.Fit(self)
      self.SetAutoLayout(1)

      self.Bind(wx.EVT_BUTTON, self.OnCancel, id=wx.ID_CANCEL)
      self.Bind(wx.EVT_BUTTON, self.OnOK, id=wx.ID_OK)


   def drawMoveErrorDialog(self):
      if self.playerIdx == 0:
         message = _("Mr. X can't move to that location.")
      elif self.playerIdx == 1:
         message = _("The Red Detective can't move to that location.")
      elif self.playerIdx == 2:
         message = _("The Yellow Detective can't move to that location.")
      elif self.playerIdx == 3:
         message = _("The Green Detective can't move to that location.")
      elif self.playerIdx == 4:
         message = _("The Blue Detective can't move to that location.")
      elif self.playerIdx == 5:
         message = _("The Black Detective can't move to that location.")
      # TRANSLATORS: this is the title of an alert window that pops up when the user tries to make an illegal move
      alert = wx.MessageDialog(self.parent, message, _("Illegal Move"), wx.OK|wx.ICON_ERROR)
      alert.ShowModal()


   def setDest1(self, destPos):
      if destPos in self.moves:
         self.Show(True)
      elif destPos != 0:
         self.Show(False)
         self.drawMoveErrorDialog()


   # after choosing a destination to move to, update the associated 
   # list of transportations
   def updateTrans1(self, event):
      self.trans    = []
      self.transStr = []
      self.updateDest2()


   def updateDest2(self):
      if self.playerIdx == 0:
         pl2 = []
         player = []
         player.append(self.playerList[0][0])
         tokenList = self.playerList[0][2][:]
         player.append(tokenList)
         pl2.append(player)
         for i in list(range(1, len(self.playerList))):
            pl2.append(self.playerList[i])

         self.moves2    = []
         self.moves2Str = []
         self.updateTrans2()
      else:
         self.sizer.Layout()


   def updateTrans2Evt(self, event):
      self.updateTrans2()

   def updateTrans2(self):
      xtokens = self.playerList[0][2][:]
      self.trans2    = []
      self.trans2Str = []
      self.sizer.Layout()



   # cancelled the dialog
   def OnCancel(self, event):
      self.destroyedCallback()
      self.Destroy()


   def OnOK(self, event):
      index2PawnName = ["purple", "red", "yellow", "green", "blue", "black"]
      move = [index2PawnName[self.playerIdx]]
      move.append("1")
      move.append("taxi")
      self.messenger.netMakeMove(move) 
      self.destroyedCallback()
      self.Destroy()



   # Returns a tuple.  The first element is a list of integer available
   # moves, the second is the same list converted to strings.
   def getAvailMoves(self, pos, playerList, playerIdx):
      tokenList = playerList[playerIdx][2]

      # These are the positions that the player could get to using his current tokens,
      # if no detectives were in the way.
      movesFromMap = []
      for routes in locToRoutes[pos]:
         if playerIdx == 0: 
            # Mr. X can always take a route with standard transports (infinite tickets)
            if (TAXI in routes[1]) or (BUS in routes[1]) or \
            (UNDERGROUND in routes[1]):
               movesFromMap.append(routes[0])
            # Mr. X also has the option of black ticket moves
            elif tokenList[3] > 0: 
               movesFromMap.append(routes[0])
         else:
            # Detectives have to pay for everything
            if ((TAXI in routes[1] and tokenList[0] > 0) or
                (BUS in routes[1] and tokenList[1] > 0) or
                (UNDERGROUND in routes[1] and tokenList[2] > 0)):
               movesFromMap.append(routes[0])

      # Remove positions that a detective is on.
      detPositions = []
      for det in playerList[1:]:
         detPositions.append(det[1])

      availMoves = []
      for move in movesFromMap:
         if (move not in detPositions) or (playerIdx == 0):
            # this is to check that the player has at least one ticket type
            # that will get him to this destination; otherwise it is
            # effectively an illegal move
            transports, transportsStr = self.getAvailTransports(pos, move, tokenList, playerIdx)
            if transports != []:
               availMoves.append(move)

      availMoves.sort()
      availMovesStr = []
      for move in availMoves:
         availMovesStr.append(str(move))

      return availMoves, availMovesStr



   # Returns a tuple.  The first element is a list of integer available
   # transports (TAXI, BUS, etc.), the second is a more pleasant
   # string version.
   def getAvailTransports(self, pos, destPos, tokenList, playerIdx):
      routes = locToRoutes[pos]
      for route in routes:
         if route[0] == destPos:
            transports = route[1]
            break

      # Mr. X has another option
      if playerIdx == 0:
         if BLACK not in transports and tokenList[3] > 0:
            transports = transports + (BLACK,)

      transportsStr = []
      for trans in transports:
         if trans == TAXI and (tokenList[0] > 0 or tokenList[0] == -1):
            # TRANSLATORS: this is used for choosing ticket type in the move dialog
            transportsStr.append(_("taxi"))
         elif trans == BUS and (tokenList[1] > 0 or tokenList[1] == -1):
            # TRANSLATORS: this is used for choosing ticket type in the move dialog
            transportsStr.append(_("bus"))
         elif trans == UNDERGROUND and (tokenList[2] > 0 or tokenList[2] == -1):
            # TRANSLATORS: this is used for choosing ticket type in the move dialog
            transportsStr.append(_("underground"))
         elif trans == BLACK and (tokenList[3] > 0):
            # TRANSLATORS: this is used for choosing ticket type in the move dialog
            transportsStr.append(_("black ticket"))

      return transports, transportsStr



