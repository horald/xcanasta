#!/usr/bin/env python
# -*- coding: UTF-8*-

import wx
import wx.adv

class TestFrame(wx.Frame):
    def __init__(self, *args, **kwds):        
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        
        self.btnShowDialog = wx.Button(self, wx.ID_ANY, "ShowDialog")
        #Button bind:
        self.btnShowDialog.Bind(wx.EVT_BUTTON, self.OnBtnShowDialog)

        self.__set_properties()
        self.__do_layout()        

    def __set_properties(self):  
        self.SetSize((400, 300))      
        self.SetTitle("Test Frame")

    def __do_layout(self):        
        sizer_1 = wx.GridSizer(1, 1, 0, 0)
        sizer_1.Add(self.btnShowDialog, 0, wx.ALIGN_CENTER, 0)
        self.SetSizer(sizer_1)
        self.Layout()
        
    def OnBtnShowDialog(self, event):
        # Shows my own dialog
        dlg = LinkDialog(self, wx.ID_ANY, title="My Custom Dialog")
        dlg.ShowModal()

# end of class TestFrame

class LinkDialog(wx.Dialog):
    def __init__(self, *args, **kwds):
        
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, *args, **kwds)
        # using wx.adv.HyperlinkCtrl
        self.hyperlink_1 = wx.adv.HyperlinkCtrl(self, wx.ID_ANY, "Go to Stackoverflow", "https://stackoverflow.com/")

        self.__set_properties()
        self.__do_layout()
        

    def __set_properties(self):        
        self.SetSize((400, 260))
        self.hyperlink_1.SetFont(wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, 0, ""))
        

    def __do_layout(self):        
        grid_sizer_7 = wx.GridSizer(1, 1, 0, 0)
        grid_sizer_7.Add(self.hyperlink_1, 0, wx.ALIGN_CENTER, 0)
        self.SetSizer(grid_sizer_7)        
        self.Layout()
        

# end of class LinkDialog

class MyApp(wx.App):
    def OnInit(self):
        self.frame = TestFrame(None, wx.ID_ANY, "")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

# end of class MyApp

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()