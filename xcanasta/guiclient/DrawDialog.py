#  XCanasta -- Network-compatible computer variant of the popular card game
#  Copyright (C) 2021 Horst Aldebaran
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License, Version 2, as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import gettext, wx


class DrawDialog(wx.Dialog):
   def __init__(self, parent, ID):
      title=_("Draw a Card")
      wx.Dialog.__init__(self, parent, ID, title)
      self.parent = parent
      self.panel = wx.Panel(self, -1)

      self.moveType = wx.RadioBox(self.panel, -1, _("draw type: "), wx.DefaultPosition, wx.DefaultSize,
            [_("Card joint"), 
            _("Discard pile")], 1, wx.RA_SPECIFY_ROWS)
      
      okButton = wx.Button(self.panel, wx.ID_OK, _("OK"))
      cancelButton = wx.Button(self.panel, wx.ID_CANCEL, _("Cancel"))
      buttonSizer = wx.BoxSizer(wx.HORIZONTAL)
      buttonSizer.Add(cancelButton, 0, wx.ALIGN_CENTRE | wx.ALL, 5)
      buttonSizer.Add(okButton, 0, wx.ALIGN_CENTRE | wx.ALL, 5)

      self.pSizer = wx.BoxSizer(wx.VERTICAL)
      self.pSizer.Add(self.moveType, 0, wx.ALIGN_CENTRE | wx.ALL, 5)
      self.pSizer.Add(buttonSizer, 0, wx.ALIGN_RIGHT | wx.ALL, 5)

      self.panel.SetSizer(self.pSizer)
      self.pSizer.Fit(self.panel)
      self.sizer = wx.BoxSizer(wx.VERTICAL)
      self.sizer.Add(self.panel, 1, wx.EXPAND | wx.ALL | wx.ADJUST_MINSIZE, 5)
      self.SetSizer(self.sizer)
      self.sizer.Fit(self)
      self.SetAutoLayout(1)
      
      self.Bind(wx.EVT_BUTTON, self.OnCancel, id=wx.ID_CANCEL)
      self.Bind(wx.EVT_BUTTON, self.OnOK, id=wx.ID_OK)
         		
   def OnCancel(self, event):
      self.Hide()

   def OnOK(self, event):
      self.Hide()
      