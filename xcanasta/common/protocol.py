#  XCanasta -- Network-compatible computer variant of the popular card game
#  Copyright (C) 2021 Horst Aldebaran
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License, Version 2, as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os, json

cwd=os.getcwd()
with open(cwd+"/version.json") as jsonfile:
   versdata = json.load(jsonfile)	

XCAN_VERSION          = versdata['versnr']

PROTOCOL_VERSION      = "2.0"
XCAN_PORT             = 7921

GAMESTATUS_NEW        = "new"
GAMESTATUS_INPROGRESS = "in progress"
GAMESTATUS_COMPLETE   = "complete"

GAMETYPE_STANDARD     = "standard"



